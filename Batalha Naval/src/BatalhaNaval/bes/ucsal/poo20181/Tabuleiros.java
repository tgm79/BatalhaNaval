package BatalhaNaval.bes.ucsal.poo20181;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Tabuleiros {
	static final int VAZIO = 0;
	static final int NAVIO = 1;
	static final int ERROU_TIRO = 2;
	static final int ACERTOU_TIRO = 3;
	static final int POSICAO_ALTURA = 0;
	static final int POSICAO_LARGURA = 1;
	static String nomeJogador1, nomeJogador2;
	static int alturaTabuleiro, larguraTabuleiro, quantidadeDeNavios, maxNavios;
	static int tabuleiroJogador1[][], tabuleiroJogador2[][];
	static Scanner sc = new Scanner(System.in);
	static int navioJogador1, navioJogador2;

	public static void iniciarQuantidadeDeNaviosJogadores() {

	}

	public static void obterTamanhoDosTabuleiros() {

		for (boolean verificar = false; !verificar;) {

			try {
				sc = new Scanner(System.in);
				System.out.println("Digite a altura do tabuleiro: ");
				alturaTabuleiro = sc.nextInt();
				System.out.println("Digite o comprimento do tabuleiro: ");
				larguraTabuleiro = sc.nextInt();
				verificar = true;
			} catch (InputMismatchException erro) {
				System.out.println("Digite um valor num�rico");
			}
			if (verificar) {
				break;
			}
		}
	}

	public static void obterNomesDosJogadores() {
		System.out.println("Digite o nome do Jogador 1: ");
		nomeJogador1 = sc.next();
		System.out.println("Digite o nome do Jogador 2: ");
		nomeJogador2 = sc.next();
	}

	public static void calcularQuantidadeMaximaDeNaviosNoJogo() {
		maxNavios = (alturaTabuleiro * larguraTabuleiro) / 3;
	}

	public static void iniciandoOsTamanhosDosTabuleiros() {
		tabuleiroJogador1 = retornarNovoTabuleiroVazio();
		tabuleiroJogador2 = retornarNovoTabuleiroVazio();
	}

	public static int[][] retornarNovoTabuleiroVazio() {
		return new int[alturaTabuleiro][larguraTabuleiro];
	}

	public static void obterQuantidadeDeNaviosNoJogo() {
		System.out.println("Digite a quantidade de navios do jogo:");
		System.out.println("Max: " + maxNavios + " navios");
		quantidadeDeNavios = sc.nextInt();
		if (quantidadeDeNavios < 1 && quantidadeDeNavios > maxNavios) {
			quantidadeDeNavios = maxNavios;
		}
	}

	public static int[][] retornarNovoTabuleiroComOsNavios() {
		int novoTabuleiro[][] = retornarNovoTabuleiroVazio();
		int quantidadeRestanteDeNavios = quantidadeDeNavios;
		int x = 0, y = 0;
		Random numeroAleatorio = new Random();
		do {
			x = 0;
			y = 0;
			for (int[] linha : novoTabuleiro) {
				for (int coluna : linha) {
					if (numeroAleatorio.nextInt(100) <= 10) {
						if (coluna == VAZIO) {
							novoTabuleiro[x][y] = NAVIO;
							quantidadeRestanteDeNavios--;
							break;
						}
						if (quantidadeRestanteDeNavios < 0) {
							break;
						}
					}
					y++;
				}
				y = 0;
				x++;
				if (quantidadeRestanteDeNavios <= 0) {
					break;
				}
			}
		} while (quantidadeRestanteDeNavios > 0);
		return novoTabuleiro;
	}

	public static void inserirOsNaviosNosTabuleirosDosJogadores() {
		tabuleiroJogador1 = retornarNovoTabuleiroComOsNavios();
		tabuleiroJogador2 = retornarNovoTabuleiroComOsNavios();
	}

	public static void exibirNumerosDasColunasDosTabuleiros() {
		int numeroDaColuna = 1;
		String numerosDoTabuleiro = "   ";

		for (int i = 0; i < larguraTabuleiro; i++) {
			numerosDoTabuleiro += (numeroDaColuna++) + " ";
		}
		System.out.println(numerosDoTabuleiro);
	}

	public static void exibirTabuleiro(String nomeDoJogador, int[][] tabuleiro, boolean seuTabuleiro) {
		System.out.println("|----- " + nomeDoJogador + " -----|");
		exibirNumerosDasColunasDosTabuleiros();
		String linhaDoTabuleiro = "";
		char letraDaLinha = 65;
		for (int[] linha : tabuleiro) {
			linhaDoTabuleiro = (letraDaLinha++) + " |";

			for (int coluna : linha) {
				switch (coluna) {
				case VAZIO:
					linhaDoTabuleiro += " |";
					break;
				case NAVIO:
					if (seuTabuleiro) {
						linhaDoTabuleiro += "N|";
						break;
					} else {
						linhaDoTabuleiro += " |";
						break;
					}
				case ERROU_TIRO:
					linhaDoTabuleiro += "X|";
					break;

				case ACERTOU_TIRO:
					linhaDoTabuleiro += "D|";
					break;
				}
			}
			System.out.println(linhaDoTabuleiro);
		}
	}

	public static void exibirTabuleirosDosJogadores() {
		exibirTabuleiro(nomeJogador1, tabuleiroJogador1, true);
		exibirTabuleiro(nomeJogador2, tabuleiroJogador2, false);
	}

	public static boolean validarPosicoesInseridasPeloJogador(int[] posicoes) {
		boolean retorno = true;
		if (posicoes[0] > alturaTabuleiro - 1) {
			retorno = false;
			System.out.println("A posicao das letras n�o pode ser maior que " + (char) (alturaTabuleiro + 64));
		}

		if (posicoes[1] > larguraTabuleiro) {
			retorno = false;
			System.out.println("A posicao num�rica n�o pode ser maior que " + larguraTabuleiro);
		}

		return retorno;
	}

	public static String receberValorDigitadoPeloJogador() {
		System.out.println("Digite a posi��o do seu tiro:");
		return sc.next();
	}

	public static boolean validarTiroDoJogador(String tiroDoJogador) {
		int quantidadeDeNumeros = (larguraTabuleiro > 10) ? 2 : 1;
		String expressaoDeVerificacao = "^[A-Za-z]{1}[0-9]{" + quantidadeDeNumeros + "}$";
		return tiroDoJogador.matches(expressaoDeVerificacao);
	}

	public static int[] retornarPosicoesDigitadasPeloJogador(String tiroDoJogador) {
		String tiro = tiroDoJogador.toLowerCase();
		int[] retorno = new int[2];
		retorno[POSICAO_ALTURA] = tiro.charAt(0) - 97;
		retorno[POSICAO_LARGURA] = Integer.parseInt(tiro.substring(1)) - 1;
		return retorno;
	}

	public static void inserirValoresDaAcaoNoTabuleiro(int[] posicoes, int numeroDoJogador) {
		if (numeroDoJogador == 1) {
			if (tabuleiroJogador2[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] == NAVIO) {
				tabuleiroJogador2[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] = ACERTOU_TIRO;
				navioJogador2--;
				System.out.println("Voc� acertou um navio!");
			} else {
				tabuleiroJogador2[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] = ERROU_TIRO;
				System.out.println("Voc� errou o tiro!");
			}
		} else {
			if (tabuleiroJogador1[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] == NAVIO) {
				tabuleiroJogador1[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] = ACERTOU_TIRO;
				navioJogador1--;
				System.out.println("Voc� acertou um navio!");
			} else {
				tabuleiroJogador1[posicoes[POSICAO_ALTURA]][posicoes[POSICAO_LARGURA]] = ERROU_TIRO;
				System.out.println("Voc� errou o tiro!");
			}
		}
	}

	public static boolean acaoDoJogador() {
		boolean acaoValida = true;
		String tiroDoJogador = receberValorDigitadoPeloJogador();
		if (validarTiroDoJogador(tiroDoJogador)) {
			int[] posicoes = retornarPosicoesDigitadasPeloJogador(tiroDoJogador);
			if (validarPosicoesInseridasPeloJogador(posicoes)) {
				inserirValoresDaAcaoNoTabuleiro(posicoes, 1);
			} else {
				acaoValida = false;
			}
		} else {
			System.out.println("Posi��o inv�lida");
			acaoValida = false;
		}
		return acaoValida;
	}

	public static void main(String[] args) {
		obterNomesDosJogadores();
		obterTamanhoDosTabuleiros();
		calcularQuantidadeMaximaDeNaviosNoJogo();
		iniciandoOsTamanhosDosTabuleiros();
		obterQuantidadeDeNaviosNoJogo();
		inserirOsNaviosNosTabuleirosDosJogadores();
		iniciarQuantidadeDeNaviosJogadores();
		boolean jogoAtivo = true;
		do {
			exibirTabuleirosDosJogadores();
			if (acaoDoJogador()) {
				Random computador = new Random();
				int[] posicoes = new int[2];
				int numeroGerado = computador.nextInt(alturaTabuleiro);

				posicoes[POSICAO_ALTURA] = (numeroGerado == alturaTabuleiro) ? --numeroGerado : numeroGerado;
				numeroGerado = computador.nextInt(larguraTabuleiro);

				posicoes[POSICAO_LARGURA] = (numeroGerado == larguraTabuleiro) ? --numeroGerado : numeroGerado;

				inserirValoresDaAcaoNoTabuleiro(posicoes, 2);
			}
		} while (jogoAtivo);
		sc.close();

	}

}